//----------------------------------------------------------------------------
// License:  LGPL
//
// Author:  Garrett Potts, ported by David Burken
//
// Description:  Byte swap code.
//
//----------------------------------------------------------------------------
// $Id$
#ifndef drbEndian_HEADER
#define drbEndian_HEADER 1

#include <Constants.h>

namespace drb
{
   drb::ByteOrder getSystemEndianType();

   class DRB_DLL_EXPORT Endian
   {
   public:
      
      Endian();
      
      
      /*!
       *  Swap bytes methods that work on a single pixel.
       */
      void swap(drb::int16   &data) const;
      void swap(drb::uint16  &data) const;
      void swap(drb::int32   &data) const;
      void swap(drb::uint32  &data) const;
      void swap(drb::uint64  &data) const;
      void swap(drb::sint64  &data) const;
      void swap(drb::float32 &data) const;
      void swap(drb::float64 &data) const;
      
      /*!
       *  Swap bytes methods that work on arrays of pixels.
       *
       *  @note size is number of pixels, not number of bytes.
       */
      void swap(drb::ScalarType scalar, void* data, drb::uint32 size) const;

      void swap(drb::sint8* data, drb::uint32 size)const;
      void swap(drb::uint8* data, drb::uint32 size)const;
      
      void swap(drb::int16* data, drb::uint32 size) const;
      void swap(drb::uint16* data, drb::uint32 size) const;
      
      void swap(drb::int32* data, drb::uint32 size) const;
      void swap(drb::uint32* data, drb::uint32 size) const;
      
      void swap(drb::int64* data, drb::uint32 size) const;
      void swap(drb::uint64* data, drb::uint32 size) const;

      void swap(drb::float32* data, drb::uint32 size) const;
      void swap(drb::float64* data, drb::uint32 size) const;
      
      void swapTwoBytes(void* data, drb::uint32 size) const;
      void swapFourBytes(void* data, drb::uint32 size) const;
      void swapEightBytes(void* data, drb::uint32 size) const;

private:

   void swapTwoBytes(void *data) const;
   void swapFourBytes(void *data) const;
   void swapEightBytes(void *data) const;
   
   void swapPrivate(drb::uint8 *c1,
                    drb::uint8 *c2) const;
   };
   
} // namespace drb

inline void drb::Endian::swap(drb::sint8* /* data */,
                              drb::uint32 /* size */ )const
{
   //intentionally left blank
}

inline void drb::Endian::swap(drb::uint8* /* data */,
                              drb::uint32 /* size */ )const
{
   //intentionally left blank
}

inline void drb::Endian::swap(drb::int16 &data) const
{
   swapTwoBytes(reinterpret_cast<void*>(&data));   
} 

inline void drb::Endian::swap(drb::uint16 &data) const
{
   swapTwoBytes(reinterpret_cast<void*>(&data));
}

inline void drb::Endian::swap(drb::int32 &data) const
{
   swapFourBytes(reinterpret_cast<void*>(&data));
}

inline void drb::Endian::swap(drb::uint32 &data) const
{
   swapFourBytes(reinterpret_cast<void*>(&data));
}

inline void drb::Endian::swap(drb::uint64 &data) const
{
   swapEightBytes(reinterpret_cast<void*>(&data));
}

inline void drb::Endian::swap(drb::sint64 &data) const
{
   swapEightBytes(reinterpret_cast<void*>(&data));
}

inline void drb::Endian::swap(drb::float32 &data) const
{
   swapFourBytes(reinterpret_cast<void*>(&data));
}

inline void drb::Endian::swap(drb::float64 &data) const
{
   swapEightBytes(reinterpret_cast<void*>(&data));
}

inline void drb::Endian::swapTwoBytes(void *data) const
{
   unsigned char *c = reinterpret_cast<unsigned char*>(data);

   swapPrivate(&c[0], &c[1]);
}

inline void drb::Endian::swapFourBytes(void* data) const
{
   unsigned char *c = reinterpret_cast<unsigned char*>(data);

   swapPrivate(&c[0], &c[3]);
   swapPrivate(&c[1], &c[2]);
}

inline void drb::Endian::swapEightBytes(void* data) const
{
   unsigned char *c = reinterpret_cast<unsigned char*>(data);

   swapPrivate(&c[0], &c[7]);
   swapPrivate(&c[1], &c[6]);
   swapPrivate(&c[2], &c[5]);
   swapPrivate(&c[3], &c[4]);
}

inline void drb::Endian::swapPrivate(drb::uint8 *c1,                       
                                     drb::uint8 *c2) const
{
   drb::uint8 temp_c = *c1;
   *c1 = *c2;
   *c2 = temp_c;
}

inline void drb::Endian::swap(drb::ScalarType scalar,
                              void* data, drb::uint32 size) const
{
   switch (scalar)
   {
      case drb::UINT16:
      case drb::SINT16:
         swapTwoBytes(data, size);
         return;
         
      case drb::FLOAT32:
         swapFourBytes(data, size);
         return;
         
      case drb::FLOAT64:
         swapEightBytes(data, size);
         break;
         
      default:
         return;
   }
}

inline void drb::Endian::swap(drb::int16* data, drb::uint32 size) const
{
   swapTwoBytes(data, size);
}

inline void drb::Endian::swap(drb::uint16* data, drb::uint32 size) const
{
   swapTwoBytes(data, size);
}

inline void drb::Endian::swap(drb::int32* data, drb::uint32 size) const
{
   swapFourBytes(data, size);
}

inline void drb::Endian::swap(drb::uint32* data, drb::uint32 size) const
{
   swapFourBytes(data, size);
}

inline void drb::Endian::swap(drb::int64* data, drb::uint32 size) const
{
   swapEightBytes(data, size);
}

inline void drb::Endian::swap(drb::uint64* data, drb::uint32 size) const
{
   swapEightBytes(data, size);
}

inline void drb::Endian::swap(drb::float32* data, drb::uint32 size) const
{
   swapFourBytes(data, size);
}

inline void drb::Endian::swap(drb::float64* data, drb::uint32 size) const
{
   swapEightBytes(data, size);
}

inline void drb::Endian::swapTwoBytes(void* data, drb::uint32 size) const
{
   drb::uint16* buf = reinterpret_cast<drb::uint16*>(data);
   for (drb::uint32 i=0; i<size; ++i)
   {
      buf[i] = ((buf[i] & 0x00ff) << 8) | ((buf[i] & 0xff00) >> 8);
   }
}

inline void drb::Endian::swapFourBytes(void* data, drb::uint32 size) const
{
   drb::uint32* buf = reinterpret_cast<drb::uint32*>(data);
   for (drb::uint32 i=0; i<size; ++i)
   {
      buf[i]
         = (  ((buf[i] & 0xff000000) >> 24)
            | ((buf[i] & 0x00ff0000) >> 8)
            | ((buf[i] & 0x0000ff00) << 8)
            | ((buf[i] & 0x000000ff) << 24));
   }
}

inline void drb::Endian::swapEightBytes(void* data, drb::uint32 size) const
{
   uint64* buf = reinterpret_cast<drb::uint64*>(data);
   for (uint32 i=0; i<size; ++i)
   {
      buf[i]
         = (  ((buf[i] & 0xff00000000000000ull) >> 56)
            | ((buf[i] & 0x00ff000000000000ull) >> 40)
            | ((buf[i] & 0x0000ff0000000000ull) >> 24)
            | ((buf[i] & 0x000000ff00000000ull) >> 8)
            | ((buf[i] & 0x00000000ff000000ull) << 8)
            | ((buf[i] & 0x0000000000ff0000ull) << 24)
            | ((buf[i] & 0x000000000000ff00ull) << 40)
            | ((buf[i] & 0x00000000000000ffull) << 56));
   }
}

#endif /* End of #ifndef drbEndian_HEADER */
