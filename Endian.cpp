//----------------------------------------------------------------------------
// License:  LGPL
//
// Author:  Garrett Potts, ported by David Burken
//
// Description:  Byte swap code.
//
//----------------------------------------------------------------------------
// $Id$

#include <Endian.h>

drb::ByteOrder drb::getSystemEndianType()
{
   drb::uint16  test;
   drb::uint8  *testPtr=0;
   test = 0x0001;
   
   testPtr = reinterpret_cast<unsigned char*>(&test);
   return testPtr[0] ? drb::DRB_LITTLE_ENDIAN : drb::DRB_BIG_ENDIAN;
}

drb::Endian::Endian()
{
}




