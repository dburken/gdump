
::---
:: File gdump-cmake-config.bat
:: Convenience script for windows typically placed in out of source 
:: build dir parallel to gdump then do:
:: ./gdump-cmake-config.sh
:: make
::
:: :: NOTES: 
:: 1) Last line of this should point to gdump.
:: 2) Installs to ${install_dir}/bin/gdump
::---

cmake -G "Ninja" ^
-DCMAKE_INSTALL_PREFIX=D:/dev ^
-DCMAKE_BUILD_TYPE=Release ^
..

