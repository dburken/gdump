# GDUMP:

Gdump is a stand alone geotiff dump utility that has zero dependencies on any other libraries.

# Building:

# Linux:

// cd into top level.
$ cd gdump
$ mkdir build
$ cd build
$ ../gdump-cmake-config.sh
$ make

// Note default install is to: ${HOME}/bin
$ make install
