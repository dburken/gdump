//---
// FILE: GeoTiff
//
// License: MIT
//
// Author:  David Burken
//
// Description:
// 
// Class definition for GeoTiff tag reader class.
//
//---
// $Id$

#include <GeoTiff.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>


// static const bool DRB_TRACE = true;
static const bool DRB_TRACE = false;

static const bool DRB_TRACE_OFFSETS = false;

static const std::string PHOTO_INTERP[] =
{
   "MINISWHITE",
   "MINISBLACK",
   "RGB",
   "PALETTE",
   "MASK",
   "SEPARATED",
   "YCBCR",
   "CIELAB"
};

namespace drb
{

GeoTiff::GeoTiff()
   : m_file(),
     m_endian(0),
     m_printUnhandled(false)
{
}

GeoTiff::~GeoTiff()
{
   if (m_endian)
   {
      delete m_endian;
      m_endian = 0;
   } 
}

bool GeoTiff::open(const std::string& file)
{
   bool result = false;

   //---
   // Open the tif file.
   //---
   std::ifstream str(file.c_str(), std::ios_base::binary|std::ios_base::in);
   if (str.good()) 
   {
      //---
      // Get the byte order.  First two byte should be "II" or "MM".
      //---
      char byteOrder[2];
      str.read(byteOrder, 2); // Read the byte order.
      ByteOrder sysByteOrder = getSystemEndianType();
      ByteOrder tifByteOrder = DRB_LITTLE_ENDIAN;
      
      if (byteOrder[0] == 'M')
      {
         tifByteOrder = DRB_BIG_ENDIAN;
      }
      
      if (sysByteOrder != tifByteOrder)
      {
         if (!m_endian)
         {
            m_endian = new Endian();
         }
      }
      else if (m_endian)
      {
         delete m_endian;
         m_endian = 0;
      }
      
      //--
      // Get the version. Note m_endian must be set/unset before calling
      // "readShort".
      //---
      drb::uint16 version;
      readShort(version, str);

      if ( ( (byteOrder[0] == 'M') || (byteOrder[0] == 'I') ) &&
           ( (version == 42) || (version == 43) ) )
      {
         result = true;  // is a tif...
      }
   }

   if (result)
   {
      m_file = file;
   }
   else
   {
      m_file.clear();
      if (m_endian)
      {
         delete m_endian;
         m_endian = 0;
      }
   }

   return result;
}

void GeoTiff::setPrintUnhandledFlag(bool flag)
{
   m_printUnhandled = flag;
}

bool GeoTiff::getPrintUnhandledFlag() const
{
   return m_printUnhandled;
}

std::ostream& GeoTiff::print(std::ostream& out) const
{
   static const char MODULE[] = "GeoTiff::print";

   if (DRB_TRACE)
   {
      out << MODULE << " DEBUG Entered...\n";
   }
      
   //---
   // Open the tif file.
   //---
   std::ifstream str(m_file.c_str(), std::ios_base::binary|std::ios_base::in);
   if (!str) 
   {
      if (DRB_TRACE)
      {
         out << MODULE << " Cannot open file:  " << m_file << std::endl;
      }
      return out;
   }
   
   //---
   // Get the byte order.  The data member "theTiffByteOrder" was set to
   // BIG_END in the initialization list.  This will be changed if
   // byte order is LITTLE_END(IBM, DEC).
   //---
   char byteOrder[2];
   str.read(byteOrder, 2); // Read the byte order.

   // Get the version:
   drb::uint16 version;
   readShort(version, str);

   // Set the tag value length.
   drb::uint64 tagValueLength;
   if (version == 42)
   {
      tagValueLength = 4;
   }
   else
   {
      tagValueLength = 8;
   }

   out << "tiff.version: " << int(version)
       << ((version==42)?"(classic)\n":"(big)\n")
       << "tiff.byte_order: ";
   
   if (byteOrder[0] == 'M')
   {
      out << "big_endian\n";
   }
   else // DRB_LITTLE_ENDIAN
   {
      out  << "little_endian\n";
   }

   if (DRB_TRACE)
   {
      out << "system_byte_order: ";
      if (getSystemEndianType() == DRB_BIG_ENDIAN)
      {
         out << "big_endian\n";
      }
      else // DRB_LITTLE_ENDIAN
      {
         out << "little_endian\n";
      }
      out
         << "tiff.tag_value_length: " << tagValueLength << "\n";
   }

   //---
   // Variables used within the loop.
   //---
   std::streamoff seekOffset;      // used throughout
   std::streampos streamPosition;  // used throughout

   if (version == 43)
   {
      // We must skip the first four bytes.
      drb::uint32 offsetSize;
      readLong(offsetSize, str);
   }

   // Get the offset.
   if (getOffset(seekOffset, str, version) == false)
   {
     out << MODULE << " FATAL ERROR - "
         << "No offset to an image file directory found.\n"
         << "Returning with error."
         << std::endl;
     str.close();
     return out;
   }
   
   if (DRB_TRACE)
   {
      out
         << MODULE << " DEBUG: "
         << "Offset to first ifd:  " << seekOffset
         << "\n";
   }

   // Capture the original flags then set float output to full precision.
   std::ios_base::fmtflags f = out.flags();
   out << std::setiosflags(std::ios::fixed) << std::setprecision(15);
   
   // Image File Directory (IFD) loop.
   int32 ifdIndex = 0;
   while(seekOffset)
   {
      out << "tiff.directory_offset: " << seekOffset << "\n";

      // Seek to the image file directory.
      str.seekg(seekOffset, std::ios_base::beg);  

      // directory prefix for prints.
      std::string prefix = "tiff.";
      getDirPrefix(ifdIndex, prefix);

      //---
      // Things we need to save for printGeoKeys:
      //---
      drb::uint16*  geoKeyBlock     = 0;
      drb::uint64   geoKeyLength    = 0;
      float64* geoDoubleBlock  = 0;
      drb::uint64   geoDoubleLength = 0;
      int8*    geoAsciiBlock   = 0;
      drb::uint64   geoAsciiLength  = 0;
      
      //---
      // Get the number of directories within the IFD.
      //---
      drb::uint64 nTags; // Number of tags in an IFD.
      if (getValue(nTags, str, TWO_OR_EIGHT, version) == false)
      {
         out << MODULE << " FATAL error reading number of direcories."
             << std::endl;
         str.close();
         return out;
      }

      if (DRB_TRACE)
      {
         out
            << MODULE << " DEBUG:\n"
            << "ifd:  " << seekOffset
            << "\ntags in directory:  " << nTags<< "\n";
      }

      // Tag loop:
      for (drb::uint64 tagIdx = 0; tagIdx < nTags; ++tagIdx)
      {
         // Variables used within the loop.
         drb::uint16   tag              = 0; // Tag number
         drb::uint16   type             = 0; // Type(short, long...)
         drb::uint64   count            = 0;
         drb::uint64   arraySizeInBytes = 0; // 
         drb::uint8*   valueArray       = 0; // To hold value.
         
         //---
         // Get the tag.
         //---
         readShort(tag, str);
         if (!str)
         {
            out << MODULE << " FATAL error reading tag number."
                << std::endl;
            str.close();
            return out;
         }

         //---
         // Get the type (byte, ascii, short...)
         //---
         readShort(type, str);
         if (!str)
         {
            out << MODULE << " FATAL error reading type number."
                << std::endl;
            str.close();
            return out;
         }

         //---
         // Get the count.  This is not in bytes.  It is based on the
         // type.  So if the type is a short and the count is one then
         // read "sizeof(short"(2) bytes.
         //---
         getValue(count, str, FOUR_OR_EIGHT, version);
         if (!str)
         {
            out << MODULE << " FATAL error reading count."
                << std::endl;
            str.close();
            return out;
         }

         // Get the array size in bytes.
         arraySizeInBytes = getArraySizeInBytes(count, type);
         if (arraySizeInBytes == 0)
         {
            // Could be an unhandle type.  Gobble the value.
            eatValue(str, version);
         }
         else
         {
            // Allocate array.
            if (valueArray) delete [] valueArray;
            valueArray = new drb::uint8[static_cast<drb::uint32>(arraySizeInBytes)];

            if (arraySizeInBytes <= tagValueLength)
            {
               // Read in the value(s).
               str.read((char*)valueArray, arraySizeInBytes);

               // Skip any byes left in the field.
               if (arraySizeInBytes < tagValueLength)
               {
                  // Skip these bytes.
                  str.ignore(tagValueLength-arraySizeInBytes);
               }
            }
            else // Data to big for field.  Stored elsewhere...
            {
               // Get the offset to the data.
               getOffset(seekOffset, str, version);

               // Capture the seek position to come back to.
               streamPosition = str.tellg();

               // Seek to the data.
               str.seekg(seekOffset, std::ios_base::beg);

               // Read in the value(s).
               str.read((char*)valueArray, arraySizeInBytes);

               // Seek back.
               str.seekg(streamPosition);
            }

            // Swap the bytes if needed.
            swapBytes(valueArray, type, count);
         }

         if( DRB_TRACE )
	 {
            out
               << MODULE << " DEBUG:"
               << "\ntag[" << tagIdx << "]:" << tag
               << "\ntype:                " << type
               << "\ncount:        " << count
               << "\narray size in bytes: " << arraySizeInBytes
               << "\n";
	 }

         if (tag == GEO_KEY_DIRECTORY_TAG)
         {
            // tag 34735 save for printGeoKeys
            geoKeyBlock = reinterpret_cast<drb::uint16*>(valueArray);
            geoKeyLength = count;
         }
         else if (tag == GEO_DOUBLE_PARAMS_TAG)
         {
            // tag 34736 save for printGeoKeys
            geoDoubleBlock  = reinterpret_cast<float64*>(valueArray);
            geoDoubleLength = count;
         }
         else if (tag == GEO_ASCII_PARAMS_TAG)
         {
            // tag 34737 save for printGeoKeys
            geoAsciiBlock   = reinterpret_cast<int8*>(valueArray);
            geoAsciiLength  = count;
         }
         else
         {
            print(out,
                  prefix,
                  tagIdx,
                  tag,
                  type,
                  count,
                  arraySizeInBytes,
                  valueArray);
            
            // Free memory if allocated...
            if (valueArray)
            {
               delete [] valueArray;
               valueArray = 0;
            }
         }
         
      } // End of tag loop.

      //---
      // If Geotiff Keys read them.
      // This had to done last since the keys could
      // have references to tags GEO_DOUBLE_PARAMS_TAG and
      // GEO_ASCII_PARAMS_TAG.
      //---
      if (geoKeyBlock)
      {
         printGeoKeys(out, prefix, geoKeyLength, geoKeyBlock,
                      geoDoubleLength,geoDoubleBlock,
                      geoAsciiLength,geoAsciiBlock);

         delete [] geoKeyBlock;
         geoKeyBlock = 0;
      }

      if (geoDoubleBlock)
      {
         delete [] geoDoubleBlock;
         geoDoubleBlock = 0;
      }
      if (geoAsciiBlock)
      {
         delete [] geoAsciiBlock;
         geoAsciiBlock = 0;
      }
      geoKeyLength = 0;
      geoDoubleLength = 0;
      geoAsciiLength = 0;

      //---
      // Get the next IFD offset.  Continue this loop until the offset is
      // zero.
      //---
      if (getOffset(seekOffset, str, version) == false)
      {
         out << MODULE << " No offset to an image file directory found.\n"
             << "Returning with error."
             << std::endl;
         str.close();
         return out;
      }
      
      if (DRB_TRACE)
      {
         out
            << "DEBUG ossimTiffInfo::readTags: "
            << "Next Image File Directory(IFD) offset = "
            << seekOffset << "\n";
      }

      ++ifdIndex; // next ifd

   } // End of loop through the IFD's.
   
   out << std::endl;
    
   str.close();

   // Reset flags.
   out.setf(f);
   
   if (DRB_TRACE)
   {
      out
         << MODULE << " DEBUG Exited..." << std::endl;
   }
   
   return out;
}

std::ostream& GeoTiff::print(std::ifstream& inStr,
                             std::ostream& outStr) const
{
   static const char MODULE[] =
      "ossimTiffInfo::print(std::ifstream&, std::ostream&)";

   if (DRB_TRACE)
   {
      outStr << MODULE << " DEBUG Entered...\n";
   }

   std::streampos startPosition = inStr.tellg();
   
   //---
   // Get the byte order.  The data member "theTiffByteOrder" was set to
   // BIG_END in the initialization list.  This will be changed if
   // byte order is LITTLE_END(IBM, DEC).
   //---
   char byteOrder[2];
   inStr.read(byteOrder, 2); // Read the byte order.

   // Get the version:
   drb::uint16 version;
   readShort(version, inStr);

   // Set the tag value length.
   drb::uint64 tagValueLength;
   if (version == 42)
   {
      tagValueLength = 4;
   }
   else
   {
      tagValueLength = 8;
   }

   outStr << "tiff.version: " << int(version)
          << ((version==42)?"(classic)\n":"(big)\n")
          << "tiff.byte_order: ";
   
   if (byteOrder[0] == 'M')
   {
      outStr << "big_endian\n";
   }
   else // DRB_LITTLE_ENDIAN
   {
      outStr  << "little_endian\n";
   }

   if (DRB_TRACE)
   {
      outStr << "system_byte_order: ";
      if (getSystemEndianType()== DRB_BIG_ENDIAN)
      {
         outStr << "big_endian\n";
      }
      else // DRB_LITTLE_ENDIAN
      {
         outStr << "little_endian\n";
      }
      outStr << "tiff.tag_value_length: " << tagValueLength << "\n";
   }

   //---
   // Variables used within the loop.
   //---
   std::streamoff seekOffset;      // used throughout
   std::streampos streamPosition;  // used throughout

   if (version == 43)
   {
      // We must skip the first four bytes.
      drb::uint32 offsetSize;
      readLong(offsetSize, inStr);
   }

   // Get the offset.
   if (getOffset(seekOffset, inStr, version) == false)
   {
     outStr << MODULE << " FATAL ERROR - "
            << "No offset to an image file directory found.\n"
            << "Returning with error."
            << std::endl;
     inStr.close();
     return outStr;
   }
   
   if (DRB_TRACE)
   {
      outStr << MODULE << " DEBUG: "
             << "Offset to first ifd:  " << seekOffset
             << "\n";
   }

   // Capture the original flags then set float output to full precision.
   std::ios_base::fmtflags f = outStr.flags();
   outStr << std::setiosflags(std::ios::fixed) << std::setprecision(15);
   
   // Image File Directory (IFD) loop.
   int32 ifdIndex = 0;
   while(seekOffset)
   {
      outStr << "tiff.directory_offset: " << seekOffset << "\n";

      // Seek to the image file directory.
      inStr.seekg(startPosition+seekOffset, std::ios_base::beg);  

      // directory prefix for prints.
      std::string prefix = "tiff.";
      getDirPrefix(ifdIndex, prefix);

      //---
      // Things we need to save for printGeoKeys:
      //---
      drb::uint16*  geoKeyBlock     = 0;
      drb::uint64   geoKeyLength    = 0;
      float64* geoDoubleBlock  = 0;
      drb::uint64   geoDoubleLength = 0;
      int8*    geoAsciiBlock   = 0;
      drb::uint64   geoAsciiLength  = 0;
      
      //---
      // Get the number of directories within the IFD.
      //---
      drb::uint64 nTags; // Number of tags in an IFD.
      if (getValue(nTags, inStr, TWO_OR_EIGHT, version) == false)
      {
         outStr << MODULE << " FATAL error reading number of direcories."
                << std::endl;
         inStr.close();
         return outStr;
      }

      if (DRB_TRACE)
      {
         outStr << MODULE << " DEBUG:\n"
                << "ifd:  " << seekOffset
                << "\ntags in directory:  " << nTags<< "\n";
      }

      // Tag loop:
      for (drb::uint64 tagIdx = 0; tagIdx < nTags; ++tagIdx)
      {
         // Variables used within the loop.
         drb::uint16   tag              = 0; // Tag number
         drb::uint16   type             = 0; // Type(short, long...)
         drb::uint64   count            = 0;
         drb::uint64   arraySizeInBytes = 0; // 
         drb::uint8*   valueArray       = 0; // To hold value.
         
         //---
         // Get the tag.
         //---
         readShort(tag, inStr);
         if (!inStr)
         {
            outStr << MODULE << " FATAL error reading tag number."
                   << std::endl;
            inStr.close();
            return outStr;
         }

         //---
         // Get the type (byte, ascii, short...)
         //---
         readShort(type, inStr);
         if (!inStr)
         {
            outStr << MODULE << " FATAL error reading type number."
                   << std::endl;
            inStr.close();
            return outStr;
         }

         //---
         // Get the count.  This is not in bytes.  It is based on the
         // type.  So if the type is a short and the count is one then
         // read "sizeof(short"(2) bytes.
         //---
         getValue(count, inStr, FOUR_OR_EIGHT, version);
         if (!inStr)
         {
            outStr << MODULE << " FATAL error reading count."
                   << std::endl;
            inStr.close();
            return outStr;
         }

         // Get the array size in bytes.
         arraySizeInBytes = getArraySizeInBytes(count, type);
         if (arraySizeInBytes == 0)
         {
            // Could be an unhandle type.  Gobble the value.
            eatValue(inStr, version);
         }
         else
         {
            // Allocate array.
            if (valueArray) delete [] valueArray;
            valueArray = new drb::uint8[static_cast<drb::uint32>(arraySizeInBytes)];

            if (arraySizeInBytes <= tagValueLength)
            {
               // Read in the value(s).
               inStr.read((char*)valueArray, arraySizeInBytes);

               // Skip any byes left in the field.
               if (arraySizeInBytes < tagValueLength)
               {
                  // Skip these bytes.
                  inStr.ignore(tagValueLength-arraySizeInBytes);
               }
            }
            else // Data to big for field.  Stored elsewhere...
            {
               // Get the offset to the data.
               getOffset(seekOffset, inStr, version);

               // Capture the seek position to come back to.
               streamPosition = inStr.tellg();

               // Seek to the data.
               inStr.seekg(startPosition+seekOffset, std::ios_base::beg);

               // Read in the value(s).
               inStr.read((char*)valueArray, arraySizeInBytes);

               // Seek back.
               inStr.seekg(streamPosition);
            }

            // Swap the bytes if needed.
            swapBytes(valueArray, type, count);
         }

         if( DRB_TRACE )
	 {
            outStr << MODULE << " DEBUG:"
                   << "\ntag[" << tagIdx << "]:" << tag
                   << "\ntype:                " << type
                   << "\ncount:        " << count
                   << "\narray size in bytes: " << arraySizeInBytes
                   << "\n";
	 }

         if (tag == GEO_KEY_DIRECTORY_TAG)
         {
            // tag 34735 save for printGeoKeys
            geoKeyBlock = reinterpret_cast<drb::uint16*>(valueArray);
            geoKeyLength = count;
         }
         else if (tag == GEO_DOUBLE_PARAMS_TAG)
         {
            // tag 34736 save for printGeoKeys
            geoDoubleBlock  = reinterpret_cast<float64*>(valueArray);
            geoDoubleLength = count;
         }
         else if (tag == GEO_ASCII_PARAMS_TAG)
         {
            // tag 34737 save for printGeoKeys
            geoAsciiBlock   = reinterpret_cast<int8*>(valueArray);
            geoAsciiLength  = count;
         }
         else
         {
            print(outStr,
                  prefix,
                  tagIdx,
                  tag,
                  type,
                  count,
                  arraySizeInBytes,
                  valueArray);
            
            // Free memory if allocated...
            if (valueArray)
            {
               delete [] valueArray;
               valueArray = 0;
            }
         }
         
      } // End of tag loop.

      //---
      // If Geotiff Keys read them.
      // This had to done last since the keys could
      // have references to tags GEO_DOUBLE_PARAMS_TAG and
      // GEO_ASCII_PARAMS_TAG.
      //---
      if (geoKeyBlock)
      {
         printGeoKeys(outStr, prefix, geoKeyLength, geoKeyBlock,
                      geoDoubleLength,geoDoubleBlock,
                      geoAsciiLength,geoAsciiBlock);

         delete [] geoKeyBlock;
         geoKeyBlock = 0;
      }

      if (geoDoubleBlock)
      {
         delete [] geoDoubleBlock;
         geoDoubleBlock = 0;
      }
      if (geoAsciiBlock)
      {
         delete [] geoAsciiBlock;
         geoAsciiBlock = 0;
      }
      geoKeyLength = 0;
      geoDoubleLength = 0;
      geoAsciiLength = 0;

      //---
      // Get the next IFD offset.  Continue this loop until the offset is
      // zero.
      //---
      if (getOffset(seekOffset, inStr, version) == false)
      {
         outStr << MODULE << " No offset to an image file directory found.\n"
                << "Returning with error."
                << std::endl;
         inStr.close();
         return outStr;
      }
      
      if (DRB_TRACE)
      {
         outStr << "DEBUG ossimTiffInfo::readTags: "
                << "Next Image File Directory(IFD) offset = "
                << seekOffset << "\n";
      }

      ++ifdIndex; // next ifd
      
   } // End of loop through the IFD's.
   
   outStr << std::endl;
    
   inStr.close();

   // Reset flags.
   outStr.setf(f);
   
   if (DRB_TRACE)
   {
      outStr << MODULE << " DEBUG Exited..." << std::endl;
   }
   
   return outStr;
}

void GeoTiff::readShort(drb::uint16& s, std::ifstream& str) const
{
   str.read((char*)&s, sizeof(s));
   if (m_endian)
   {
      m_endian->swap(s);
   }
}

void GeoTiff::readLong(drb::uint32& l, std::ifstream& str) const
{
   str.read((char*)&l, sizeof(l));
   if (m_endian)
   {
      m_endian->swap(l);
   }
}

void GeoTiff::readLongLong(drb::uint64& l, std::ifstream& str) const
{
   str.read((char*)&l, sizeof(l));
   if (m_endian)
   {
      m_endian->swap(l);
   }
}

bool GeoTiff::getOffset(std::streamoff& offset,
                        std::ifstream& str,
                        drb::uint16 version) const
{
   bool status = true;
   if  (version == 42)
   {
      drb::uint32 littleOffset;
      readLong(littleOffset, str);
      offset = littleOffset;
   }
   else
   {
      drb::uint64 bigOffset;
      readLongLong(bigOffset, str);
      offset = bigOffset;
   }
   if (!str)
   {
      status = false;
   }
   return status;
}

bool GeoTiff::getValue(drb::uint64& value,
                       std::ifstream& str,
                       WordType type,
                       drb::uint16 version) const
{
   bool status = true;
   if  (version == 42)
   {
      if (type == TWO_OR_EIGHT)
      {
         drb::uint16 i;
         readShort(i, str);
         value = i;
      }
      else
      {
         drb::uint32 i;
         readLong(i, str);
         value = i;
      }
   }
   else
   {
      drb::uint64 i;
      readLongLong(i, str);
      value = i;
   }
   if (!str)
   {
      status = false;
   }
   return status;
}

drb::uint64 GeoTiff::getArraySizeInBytes(drb::uint64 length,
                                         drb::uint16 type) const
{
   return length * getTypeByteSize(type);
}

drb::uint16 GeoTiff::getTypeByteSize(drb::uint16 type) const
{
   drb::uint16 result = 0;
   
   switch (type)
   {
      case TIFF_BYTE:
      case TIFF_ASCII:
      case TIFF_SBYTE:
      case TIFF_UNDEFINED:
      {
         result = 1;
         break;
      }
         
      case TIFF_SHORT:
      case TIFF_SSHORT:
      {
         result = 2;
         break;
      }
         
      case TIFF_LONG:
      case TIFF_SLONG:
      case TIFF_IFD:
      case TIFF_FLOAT:
      {
         result = 4;
         break;
      }
         
      case TIFF_RATIONAL:
      case TIFF_SRATIONAL:
      case TIFF_DOUBLE:
      case 16:             // TIFF_LONG8 defined in big tiff only.
      case 17:             // TIFF_SLONG8 defined in big tiff only.
      case 18:             // TIFF_IFD8 defined in big tiff only.
      {
         result = 8;
         break;
      }
      default:
      {
         if (m_printUnhandled)
         {
            std::cout
               << "GeoTiff::getTypeByteSize DEBUG:"
               << "\nUnhandled type: " << int(type) << std::endl;
         }
         break;
      }
   }

   return result;
}

void GeoTiff::eatValue(std::ifstream& str, drb::uint16 version) const
{
   if (version == 42)
   {
      drb::uint32 dummy;
      readLong(dummy, str);
   }
   else
   {
      drb::uint64 dummy;
      readLongLong(dummy, str);
   }
}

void GeoTiff::swapBytes(drb::uint8* v,
                        drb::uint16 type,
                        drb::uint64 count) const
{
   if (m_endian)
   {
      drb::uint32 byteSize = getTypeByteSize(type);
      switch(byteSize)
      {
         case 2:
            m_endian->swapTwoBytes(v, static_cast<drb::uint32>(count));
            break;
         case 4:
            m_endian->swapFourBytes(v, static_cast<drb::uint32>(count));
            break;
         case 8:
            m_endian->swapEightBytes(v, static_cast<drb::uint32>(count));
            break;
         default:
            break;
      }
   }
}

template <class T> void GeoTiff::getArrayValue(
   T& v,
   drb::uint8* array,
   drb::uint64 position) const
{
   T* p = (T*)array;
   v = p[position]; // +position);
}

std::ostream& GeoTiff::print(std::ostream& out,
                             std::string& prefix,
                             drb::uint64 tagIdx,
                             drb::uint16 tag,
                             drb::uint16 type,
                             drb::uint64 count,
                             drb::uint64 arraySizeInBytes,
                             drb::uint8* valueArray) const
{
   switch(tag)
   {
      case TIFFTAG_SUBFILETYPE: // tag 254
      {
         out << prefix << "sub_file_type: : ";
         printValue(out, type, valueArray);
         break;
      }
      case TIFFTAG_IMAGEWIDTH: // tag 256
      {
         out << prefix << "image_width: ";
         printValue(out, type, valueArray);
         break;
      }
      
      case TIFFTAG_IMAGELENGTH: // tag 257
      {
         out << prefix << "image length: ";
         printValue(out, type, valueArray);
         break;
      }

      case TIFFTAG_BITSPERSAMPLE: // tag 258
      {
         out << prefix << "bits_per_sample: ";
         if (count == 1)
         {
            printValue(out, type, valueArray);
         }
         else if (valueArray)
         {
            printArray(out, type, count, valueArray);
         }
         break;
      }
         
      case TIFFTAG_COMPRESSION: // tag 259
      {
         if ( (count == 1) && (type == TIFF_SHORT) )
         {
            out << prefix << "compression: ";

            drb::uint16 s;
            getArrayValue(s, valueArray, 0);
            out << s << "\n";

            out << prefix << "compression_string: ";
            if (s == COMPRESSION_NONE )
            {
               out << "none\n";
            }
            else if ( s == COMPRESSION_CCITT_2 )
            {
               out << "ccitt_2\n";
            }
            else if ( s == COMPRESSION_CCITT_3 )
            {
               out << "ccitt_3\n";
            }
            else if ( s == COMPRESSION_CCITT_4 )
            {
               out << "ccitt_4\n";
            }
            else if ( s == COMPRESSION_DEFLATE )
            {
               out << "deflate\n";
            }
            else if ( s == COMPRESSION_LZW )
            {
               out << "lzw\n";
            }
            else if ( s == COMPRESSION_LZW )
            {
               out << "lzw\n";
            }
            else if ( s == COMPRESSION_OJPEG )
            {
               out << "ojpeg\n";
            }
            else if ( s == COMPRESSION_JPEG )
            {
               out << "jpeg\n";
            }
            else if ( s == COMPRESSION_ADOBE_DEFLATE )
            {
               out << "adobe_deflate\n";
            }
            else if ( s == COMPRESSION_PACKBIT )
            {
               out << "packits\n";
            }
            else
            {
               out << "unknown\n";
            }
         }
         break;
      }

      case TIFFTAG_PHOTOMETRIC: // tag 262
      {
         out << prefix << "photo_interpretation: ";

         if ( (count == 1) && (type == TIFF_SHORT) )
         {
            drb::uint16 s;
            getArrayValue(s, valueArray, 0);
            out << s << "\n";

            out << prefix << "photo_interpretation_string: ";
            if (s <= PHOTO_CIELAB)
            {
               out << PHOTO_INTERP[s] << std::endl;
            }
            else
            {
               out <<"range error!" << std::endl;
            }
            break;
         }
      }

      case TIFFTAG_IMAGEDESCRIPTION: // tag 270
      {
         out << prefix << "image_description: ";
         printArray(out, type, count, valueArray);
         break;
      }

      case TIFFTAG_STRIPOFFSETS: // tag 273
      {
         if (DRB_TRACE_OFFSETS)
         {
            out << prefix << "bytes_per_strip: ";
            
            if (count == 1)
            {
               printValue(out, type, valueArray);
            }
            else
            {
               printArray(out, type, count, valueArray);
            }
         }
         break;
      }
      
      case TIFFTAG_ORIENTATION: // tag 274
      {
         drb::uint16 code;
         getArrayValue(code, valueArray, 0);
         printOrientation(out, prefix, code);
         break;
      }
      
      case TIFFTAG_SAMPLESPERPIXEL: // tag 277
      {
         out << prefix << "samples_per_pixel: ";
         printValue(out, type, valueArray);
         break;
      }

      case TIFFTAG_ROWSPERSTRIP: // tag 278
      {
         out << prefix << "rows_per_strip: ";
         printValue(out, type, valueArray);
         break;
      }

      case TIFFTAG_STRIPBYTECOUNTS: // tag 279
      {
         if (DRB_TRACE_OFFSETS)
         {
            out << prefix << "bytes_per_strip: ";
            
            if (count == 1)
            {
               printValue(out, type, valueArray);
            }
            else
            {
               printArray(out, type, count, valueArray);
            }
         }
         break;
      }
      
      case TIFFTAG_MINSAMPLEVALUE: // tag 280
      {
         out << prefix << "min_sample_value: ";
         printValue(out, type, valueArray);
         break;
      }
      
      case TIFFTAG_MAXSAMPLEVALUE: // tag 281
      {
         out << prefix << "max_sample_value: ";
         printValue(out, type, valueArray);
         break;
      }
      
      case TIFFTAG_XRESOLUTION: // tag 282
      {
         out << prefix << "xresolution: ";
         printValue(out, type, valueArray);
         break;
      }
      
      case TIFFTAG_YRESOLUTION: // tag 283
      {
         out << prefix << "yresolution: ";
         printValue(out, type, valueArray);
         break;
      }
      
      case TIFFTAG_PLANARCONFIG: // tag 284
      {
         if ( (count == 1) && (type == TIFF_SHORT) )
         {
            out << prefix << "planar_configuration: ";
            drb::uint16 v;
            
            getArrayValue(v, valueArray, 0);
            if (v == 1)
            {
               out << "single image plane\n";
            }
            else if (v == 2)
            {
               out << "separate image planes\n";
            }
            else
            {
               out << "unknown planar value!\n";
            }
         }
         break;
      }

      case TIFFTAG_RESOLUTIONUNIT: // tag 296
      {
         out << prefix << "resolution_units: ";
         drb::uint16 v;
         getArrayValue(v, valueArray, 0);
         if (v == 2) 
         {
            out << "inch\n";
         }
         else if (v == 3)
         {
            out << "cm\n";
         }
         else
         {
            out << "none\n";
         }
         break;
      }

      case TIFFTAG_PAGENUMBER: // tag 297
      {
         if ( (count == 2) && (type == TIFF_SHORT) )
         {
            out << prefix << "page_number: ";
            drb::uint16 s;
            getArrayValue(s, valueArray, 0);
            out << s << "\n";
            out << prefix << "total_pages: ";
            getArrayValue(s, valueArray, 1);
            out << s << "\n";
         }
         break;
      }
      
      case TIFFTAG_SOFTWARE: // tag 305
      {
         out << prefix << "software: ";
         printArray(out, type, count, valueArray);
         break;
      }
      case TIFFTAG_DATETIME: // tag 306
      {
         out << prefix << "date_time: ";
         printArray(out, type, count, valueArray);
         break;
      }

      case TIFFTAG_ARTIST: // tag 315
      {
         out << prefix << "artist: ";
         printArray(out, type, count, valueArray);
         break;
      }
      
      case TIFFTAG_PREDICTOR: // tag 317
      {
         out << prefix << "predictor: ";
         printValue(out, type, valueArray);
         break;
      }
      case TIFFTAG_TILEWIDTH: // tag 322
      {
         out << prefix << "tile_width: ";
         printValue(out, type, valueArray);
         break;
      }

      case TIFFTAG_TILELENGTH: // tag 323
      {
         out << prefix << "tile_length: ";
         printValue(out, type, valueArray);
         break;
      }

      case TIFFTAG_TILEOFFSETS: // tag 324
      {
         if (DRB_TRACE_OFFSETS)
         {
            out << prefix << "tile_offsets: ";
            if (count == 1)
            {
               printValue(out, type, valueArray);
            }
            else
            {
               printArray(out, type, count, valueArray);
            }
         }
         break;
      }
      
      case TIFFTAG_TILEBYTECOUNTS: // tag 325
      {
         if (DRB_TRACE_OFFSETS)
         {
            out << prefix << "tile_byte_counts: ";
            if (count == 1)
            {
               printValue(out, type, valueArray);
            }
            else
            {
               printArray(out, type, count, valueArray);
            }
         }
         break;
      }

      case TIFFTAG_SUBIFD: // tag 330
      {
         if ( (count == 1) && (type == TIFF_IFD8) )
         {
            out << prefix << "subimage_descriptor: ";
            drb::uint64 v;
            getArrayValue(v, valueArray, 0);
            out << v << "\n";
         }
         else
         {
            out << prefix << "tag 330 unhandled condition.\n";
         }
         break;
      }
      
      case TIFFTAG_EXTRASAMPLES: // tag 338
      {
         out << prefix << "extra_samples: ";
         drb::uint16 v;
         getArrayValue(v, valueArray, 0);
         switch (v)
         {
            case 1:
            {
               out << "associated_alpha_data\n";
               break;
            }
            case 2:
            {
               out << "unassociated_alpha_data\n";
               break;
            }
            default:
            {
               out << "unspecified_data\n";
               break;
            }
         }
         break;
      }

      case TIFFTAG_SAMPLEFORMAT: // tag 339
      {
         out << prefix << "sample_format: ";

         if (count == 1)
         {
            printValue(out, type, valueArray);
         }
         else if (valueArray)
         {
            printArray(out, type, count, valueArray);
         }
         for (drb::uint64 i = 0; i < count; ++i)
         {
            std::ostringstream s;
            s << "sample_format_string";
            if (count > 1)
            {
               s << i;
            }
            out << prefix << s.str() << ": ";
            
            drb::uint16 v;
            getArrayValue(v, valueArray, i);
            switch (v)
            {
               case SAMPLEFORMAT_UINT:
                  out << "unsigned integer data\n";
                  break;
               case SAMPLEFORMAT_INT:
                  out << "signed integer data\n";
                  break;
               case SAMPLEFORMAT_IEEEFP:
                  out << "IEEE floating point data\n";
                  break;
               case SAMPLEFORMAT_COMPLEXINT:
                  out << "complex signed int\n";
                  break;
               case SAMPLEFORMAT_COMPLEXIEEEFP:
                  out << "complex ieee floating\n";
                  break;
               case SAMPLEFORMAT_VOID:
               default:
                  out << "untyped data\n";
                  break;
            }
         }
         break;
      }
   
      case TIFFTAG_SMINSAMPLEVALUE: // tag 340
      {
         out << prefix << "smin_sample_value: ";
         printValue(out, type, valueArray);
         break;
      }

      case TIFFTAG_SMAXSAMPLEVALUE: // tag 341
      {
         out << prefix << "smax_sample_value: ";
         printValue(out, type, valueArray);
         break;
      }

#if 0 /* TODO: Required xml parse. */
      case TIFFTAG_XMLPACKET: // tag 700
      {
         printXmpMetadata(out, prefix, count, valueArray);
         break;
      }
#endif

      case TIFFTAG_COPYRIGHT: // tag 33432
      {
         out << prefix << "copyright: ";
         printArray(out, type, count, valueArray);
         break;
      }
      
      case MODEL_PIXEL_SCALE_TAG: // tag 33550
      {
         out << prefix << "model_pixel_scale: ";
         printArray(out, type, count, valueArray);
         break;
      }

      case MODEL_TIE_POINT_TAG: // tag 33992
      {
         out << prefix << "model_tie_point: ";
         printArray(out, type, count, valueArray);
         break;
      }

      case MODEL_TRANSFORM_TAG: // tag 34264
      {
         out << prefix << "model_transform: ";
         printArray(out, type, count, valueArray);
         break;
      }

      case TIFFTAG_PHOTOSHOP:  // tag 34377
      {
         out << prefix << "photoshop_image_resource_blocks: found\n";
         break;
      }
      
      case GEO_DOUBLE_PARAMS_TAG: // tag 34736
      {
         out << prefix << "double_params: ";
         printArray(out, type, count, valueArray);
         break;
      }

      case GEO_ASCII_PARAMS_TAG: // tag 34737
      {
         out << prefix << "ascii_params: ";
         printArray(out, type, count, valueArray);
         break;
      }

#if 0 /* TODO: Required xml parse. */  
      case GDAL_METADATA_TAG: // tag  42112
      {
         printGdalMetadata(out, prefix, count, valueArray);
         break;
      }
#endif

      case GDAL_NODATA: // tag 42113
      {
         out << prefix << "gdal_nodata: ";
         printArray(out, type, count, valueArray);
         break;
      }

      case RPC_COEFFICIENT_TAG: // tag 50844
      {
         printRpcs(out, prefix, type, count, valueArray);
         break;
      }
 
      default:
      {
         out << prefix << "unhandled_tag: " << tag << "\n";
         if (m_printUnhandled)
         {
            out << "generic:"
                << "\ntag[" << tagIdx << "]:         " << tag
                << "\ntype:                " << type
                << "\ncount:        " << count
                << "\narray size in bytes: " << arraySizeInBytes
                << std::endl;
            printArray(out, type, count, valueArray);
         }
         break;
      }

   } // end of switch on tag...

   return out;
   
} // end of print

std::ostream& GeoTiff::printValue(std::ostream& out,
                                       drb::uint16 type,
                                       drb::uint8* valueArray) const
{
   switch (type)
   {
      case TIFF_SHORT:
      {
         drb::uint16 v;
         getArrayValue(v, valueArray, 0);
         out << v << "\n";
         break;
      }
      case TIFF_SSHORT:
      {
         sint16 v;
         getArrayValue(v, valueArray, 0);
         out << v << "\n";
         break;
      }
      case TIFF_LONG:
      {
         drb::uint32 v;
         getArrayValue(v, valueArray, 0);
         out << v << "\n";
         break;
      }
      case TIFF_LONG8:
      {
         drb::uint64 v;
         getArrayValue(v, valueArray, 0);
         out << v << "\n";
         break;
      }
      case TIFF_RATIONAL: // two longs first=numerator, second=denominator
      {
         drb::uint32 num;
         drb::uint32 den;
         
         getArrayValue(num, valueArray, 0);
         getArrayValue(den, valueArray, 1);
         
         // out << num << " / " << den << "\n";
         out << (num/den) << "\n";
         
         break;
         
      }
      case TIFF_SLONG:
      {
         drb::sint32 v;
         getArrayValue(v, valueArray, 0);
         out << v << "\n";
         break;
      }
      case TIFF_SLONG8:
      {
         drb::sint64 v;
         getArrayValue(v, valueArray, 0);
         out << v << "\n";
         break;
      }
      case TIFF_FLOAT:
      {
         drb::float32 v;
         getArrayValue(v, valueArray, 0);
         out << v << "\n";
         break;
      }
      case TIFF_DOUBLE:
      {
         drb::float64 v;
         getArrayValue(v, valueArray, 0);
         out << v << "\n";
         break;
      } 
      default:
      {
         out << "unhandled type: " << type << "\n";
         break;
      }
   }
   return out;
}

std::ostream& GeoTiff::printArray(std::ostream& out,
                                       drb::uint16 type,
                                       drb::uint64 arraySizeInBytes,
                                       drb::uint8* valueArray) const
{
   if (valueArray)
   {
      switch (type)
      {
         case TIFF_BYTE:
         {
            for (drb::uint64 i = 0; i < arraySizeInBytes; ++i)
            {
               out << ((drb::uint8)valueArray[i]);
            }
            out << "\n";
            break;
         }

         case TIFF_ASCII:
         {
            for (drb::uint64 i = 0; i < arraySizeInBytes; ++i)
            {
               out << ((char)valueArray[i]);
            }
            out << "\n";
            break;
         }
         case TIFF_SHORT:
          {
             drb::uint16* p = (drb::uint16*)valueArray;
             for (drb::uint64 i = 0; i < arraySizeInBytes; ++i)
             {
                out << p[i] << " ";
            }
            out << "\n";
            break;
         }
         case TIFF_LONG:
          {
             drb::uint32* p = (drb::uint32*)valueArray;
             for (drb::uint64 i = 0; i < arraySizeInBytes; ++i)
             {
                out << p[i] << " ";
            }
            out << "\n";
            break;
         }
         case TIFF_DOUBLE:
          {
             float64* p = (float64*)valueArray;
             for (drb::uint64 i = 0; i < arraySizeInBytes; ++i)
             {
                out << p[i] << " ";
            }
            out << "\n";
            break;
         }
          
         default:
         {
            out << "unhandled type: " << type << "\n";
            break;
         }
      }
   }
   else
   {
      out << "null array passed to GeoTiff::printArray method." << "\n";
   }
   return out;
}

#if 0 /* TODO: Required xml parse. */
std::ostream& drb::GeoTiff::printGdalMetadata(std::ostream& out,
                                              const std::string& prefix,
                                              drb::uint64 count,
                                              drb::uint8* valueArray) const
{
   ossimString xmlString(valueArray, valueArray+count);
   ossimRefPtr<ossimXmlNode> xmlNode = new ossimXmlNode();
   std::istringstream in(xmlString);
   if(xmlNode->read(in))
   {
      const ossimXmlNode::ChildListType& children = xmlNode->getChildNodes();
      
      ossim_uint32 idx = 0;
      for(idx = 0; idx < children.size(); ++idx)
      {
         out << prefix << "gdalmetadata."
             << ossimString(children[idx]->getAttributeValue("name")).downcase()
             << ":" << children[idx]->getText() << std::endl;
      }
   }
   return out;
}
#endif

#if 0 /* TODO: Required xml parse. */
std::ostream& ossimTiffInfo::printXmpMetadata(std::ostream& out,
                                              const std::string& prefix,
                                              ossim_uint64 count,
                                              ossim_uint8* valueArray) const
{
   ossimString xmlString(valueArray, valueArray+count);
   ossimRefPtr<ossimXmlNode> xmlNode = new ossimXmlNode();
   std::istringstream in(xmlString);

   ossimRefPtr<ossimXmlDocument> xdoc = new ossimXmlDocument();

   // Read the xml document:
   if ( xdoc->read( in ) )
   {
      std::vector<ossimRefPtr<ossimXmlNode> > xnodes;

      // Wavelength:
      ossimString path = "/x:xmpmeta/rdf:RDF/rdf:Description/Camera:CentralWavelength";
      ossimString result;
      xdoc->findNodes(path, xnodes);
      if ( xnodes.size() == 1 ) // Error if more than one.
      {
         if ( xnodes[0].valid() )
         {
            result = xnodes[0]->getText();
            if ( result.size() )
            {
               out << prefix << "xmp.camera.central_wavelength: " << result << "\n";
            }
         }
      }

      xnodes.clear();
      path = "/x:xmpmeta/rdf:RDF/rdf:Description/Camera:BandName";
      xdoc->findNodes(path, xnodes);
      if ( xnodes.size() == 1 ) // Error if more than one.
      {
         if ( xnodes[0].valid() )
         {
            result = xnodes[0]->getText();
            if ( result.size() )
            {
               out << prefix << "xmp.camera.band_name: " << result << "\n";
            }
         }
      }

      xnodes.clear();
      path = "/x:xmpmeta/rdf:RDF/rdf:Description/Camera:WavelengthFWHM";
      xdoc->findNodes(path, xnodes);
      if ( xnodes.size() == 1 ) // Error if more than one.
      {
         if ( xnodes[0].valid() )
         {
            result = xnodes[0]->getText();
            if ( result.size() )
            {
               out << prefix << "xmp.camera.wavelength_fwhm: " << result << "\n";
            }
         }
      }

      xnodes.clear();
      path = "/x:xmpmeta/rdf:RDF/rdf:Description/Camera:BandSensitivity";
      xdoc->findNodes(path, xnodes);
      if ( xnodes.size() == 1 ) // Error if more than one.
      {
         if ( xnodes[0].valid() )
         {
            result = xnodes[0]->getText();
            if ( result.size() )
            {
               out << prefix << "xmp.camera.band_sensitivity: " << result << "\n";
            }
         }
      }

      xnodes.clear();
      path = "/x:xmpmeta/rdf:RDF/rdf:Description/Camera:RigCameraIndex";
      xdoc->findNodes(path, xnodes);
      if ( xnodes.size() == 1 ) // Error if more than one.
      {
         if ( xnodes[0].valid() )
         {
            result = xnodes[0]->getText();
            if ( result.size() )
            {
               out << prefix << "xmp.camera.rig_camera_index: " << result << "\n";
            }
         }
      }

      xnodes.clear();
      path = "/x:xmpmeta/rdf:RDF/rdf:Description/Camera:Yaw";
      xdoc->findNodes(path, xnodes);
      if ( xnodes.size() == 1 ) // Error if more than one.
      {
         if ( xnodes[0].valid() )
         {
            result = xnodes[0]->getText();
            if ( result.size() )
            {
               out << prefix << "xmp.camera.yaw: " << result << "\n";
            }
         }
      }     

      xnodes.clear();
      path = "/x:xmpmeta/rdf:RDF/rdf:Description/Camera:Pitch";
      xdoc->findNodes(path, xnodes);
      if ( xnodes.size() == 1 ) // Error if more than one.
      {
         if ( xnodes[0].valid() )
         {
            result = xnodes[0]->getText();
            if ( result.size() )
            {
               out << prefix << "xmp.camera.pitch: " << result << "\n";
            }
         }
      }

      xnodes.clear();
      path = "/x:xmpmeta/rdf:RDF/rdf:Description/Camera:Roll";
      xdoc->findNodes(path, xnodes);
      if ( xnodes.size() == 1 ) // Error if more than one.
      {
         if ( xnodes[0].valid() )
         {
            result = xnodes[0]->getText();
            if ( result.size() )
            {
               out << prefix << "xmp.camera.roll: " << result << "\n";
            }
         }
      }
   }
   
   return out;
}
#endif

std::ostream& GeoTiff::printGeoKeys(
   std::ostream& out,
   std::string&  prefix,
   drb::uint64   geoKeyLength,
   drb::uint16*  geoKeyBlock,
   drb::uint64   geoDoubleLength,
   float64* geoDoubleBlock,
   drb::uint64   geoAsciiLength,
   int8*    geoAsciiBlock) const
{
   if (geoKeyLength && geoKeyBlock)
   {
      //---
      // Length passed in is the total number of shorts in the geo key
      // directory.  Each key has four short values; hence, "length/4".
      //---
      int32 index = 0;
      int32 tagCount = static_cast<int32>(geoKeyLength/4);
      for (int32 i = 0; i < tagCount; ++i)
      {
         //---
         // Each key contains four unsigned shorts:
         // GeoKey ID
         // TIFF Tag ID or 0
         // GeoKey value count
         // value or tag offset
         //---
         drb::uint16 key   = geoKeyBlock[index++];
         drb::uint16 tag   = geoKeyBlock[index++];
         drb::uint16 count = geoKeyBlock[index++];
         drb::uint16 code  = geoKeyBlock[index++];

         if (DRB_TRACE)
         {
            out
            << "DEBUG  GeoTiff::readGeoKeys"
            << "\nKey index:  " << i
            << "\ngeo key:  " << key
            << "\ntag:      " << tag
            << "\ncount:    " << count
            << "\ncode:     " << code
            << "\n";
         }

         switch (key)
         {
            case GT_MODEL_TYPE_GEO_KEY:  // key 1024 Section 6.3.1.1 Codes
            {
               printModelType(out, prefix, code);
               break;
            }               
            case GT_RASTER_TYPE_GEO_KEY:  // key 1025 Section 6.3.1.2 Code
            {
               printRasterType(out, prefix, code);
               break;
            }
            
            case GT_CITATION_GEO_KEY: // key 1026
            {
               if (tag == 34737) // using ascii array
               {
                  if (geoAsciiBlock && ( (code+count) <= geoAsciiLength ) )
                  {
                     std::string s;
                     int i = 0;
                     while (i < count)
                     {
                        s.push_back(geoAsciiBlock[code+i]);
                        ++i;
                     }
                     out << prefix << "gtiff_citation: " << s << "\n";
                  }
               }
               break;
            }

            case GEOGRAPHIC_TYPE_GEO_KEY:  // key 2048  Section 6.3.2.1 Codes
            {
               out << prefix << "gtiff_gcs_type: " << code << "\n";
               break;
            }
            
            case GEOG_CITATION_GEO_KEY: // key 2049
            {
               if (tag == 34737) // using ascii array
               {
                  if (geoAsciiBlock && ( (code+count) <= geoAsciiLength ) )
                  {
                     std::string s;
                     int i = 0;
                     while (i < count)
                     {
                        s.push_back(geoAsciiBlock[code+i]);
                        ++i;
                     }
                     out << prefix << "geographic_citation: " << s << "\n";
                  }
               }
               break;
            }
            
            case GEOG_GEODETIC_DATUM_GEO_KEY:// key 2050  Section 6.3.2.2 Codes
            {
               out << prefix << "gtiff_geodetic_datum: " << code << "\n";
               break;
            }

            case GEOG_LINEAR_UNITS_GEO_KEY:// key 2052  Section 6.3.1.3 Codes
            {
               out << prefix << "linear_units_code: " << code << "\n";
               printLinearUnits(out, prefix, std::string("linear_units_code"), code);
               break;
            }
            
            case GEOG_ANGULAR_UNITS_GEO_KEY:// key 2054  Section 6.3.1.4 Codes
            {
               out << prefix << "gtiff_angular_units_code: " << code << "\n";
               printAngularUnits(out, prefix, code);
               break;
            }

            case GEOG_ELLIPSOID_GEO_KEY:// key 2056  Section 6.3.23 Codes
            {
               out << prefix << "ellipsoid_code: " << code << "\n";
               break;
            }
            
            case GEOG_SEMI_MAJOR_AXIS: // key 2057
            {
               if (tag == 34736) // using double array
               {
                  // Code is index into array.
                  if ( geoDoubleBlock && (code < geoDoubleLength) )
                  {
                     // Always count of one.
                     out << prefix << "gtiff_semi_major_axis: "
                         << geoDoubleBlock[code] << "\n";
                  }
               }
               break;
            }
            
            case GEOG_SEMI_MINOR_AXIS: // key 2058
            {
               if (tag == 34736) // using double array
               {
                  // Code is index into array.
                  if ( geoDoubleBlock && ( code < geoDoubleLength) )
                  {
                     // Always count of one.
                     out << prefix << "gtiff_semi_minor_axis: "
                         << geoDoubleBlock[code] << "\n";
                  }
               }
               break;
            }

            case PROJECTED_CS_TYPE_GEO_KEY: // key 3072 Section 6.3.3.1 codes
            {
               out << prefix << "gtiff_pcs_code: " << code << "\n";
               break;
            }

            case PCS_CITATION_GEO_KEY: // key 3073 ascii
            {
               if (tag == 34737) // using ascii array
               {
                  if (geoAsciiBlock && ( (code+count) <= geoAsciiLength ) )
                  {
                     std::string s;
                     int i = 0;
                     while (i < count)
                     {
                        s.push_back(geoAsciiBlock[code+i]);
                        ++i;
                     }
                     out << prefix << "gtiff_pcs_citation: " << s << "\n";
                  }
               }
               
               break;
            }
      
            case PROJECTION_GEO_KEY: // key 3074 Section 6.3.3.2 codes
            {
               out << prefix << "gtiff_proj_code: " << code << "\n";
               break;
            }
            
            case PROJ_COORD_TRANS_GEO_KEY:  // key 3075 Section 6.3.3.3 codes
            {
               out << prefix << "gtiff_coord_trans_code: " << code << "\n";
               break;
            }
         
            case LINEAR_UNITS_GEO_KEY:  // key 3076 Section 6.3.1.3 codes
            {
               out << prefix << "gtiff_linear_units_code: " << code << "\n";
               printLinearUnits(out, prefix, std::string("gtiff_linear_units"), code);
               break;
            }

            case PROJ_STD_PARALLEL1_GEO_KEY:  // key 3078
            {
               if (tag == 34736) // using double array
               {
                  // Code is index into array.
                  if ( geoDoubleBlock && (code < geoDoubleLength) )
                  {
                     // Always count of one.
                     out << prefix << "gtiff_std_parallel1: "
                         << geoDoubleBlock[code] << "\n";
                  }
               }
               break;
            }
         
            case PROJ_STD_PARALLEL2_GEO_KEY:  // key 3079
            {
               if (tag == 34736) // using double array
               {
                  // Code is index into array.
                  if ( geoDoubleBlock && ( code < geoDoubleLength) )
                  {
                     // Always count of one.
                     out << prefix << "gtiff_std_parallel2: "
                         << geoDoubleBlock[code] << "\n";
                  }
               }
               break;
            }
         
            case PROJ_NAT_ORIGIN_LONG_GEO_KEY:  // key 3080
            {
               if (tag == 34736) // using double array
               {
                  // Code is index into array.
                  if ( geoDoubleBlock && ( code < geoDoubleLength) )
                  {
                     // Always count of one.
                     out << prefix << "gtiff_origin_longitude: "
                         << geoDoubleBlock[code] << "\n";
                  }
               }
               break;
            }
               
            case PROJ_NAT_ORIGIN_LAT_GEO_KEY:  // key 3081
            {
               if (tag == 34736) // using double array
               {
                  // Code is index into array.
                  if ( geoDoubleBlock && ( code < geoDoubleLength) )
                  {
                     // Always count of one.
                     out << prefix << "gtiff_origin_latitude: "
                         << geoDoubleBlock[code] << "\n";
                  }
               }
               break;
            }

            case PROJ_FALSE_EASTING_GEO_KEY:  // key 3082
            {
               if (tag == 34736) // using double array
               {
                  // Code is index into array.
                  if ( geoDoubleBlock && ( code < geoDoubleLength) )
                  {
                     // Always count of one.
                     out << prefix << "gtiff_false_easting: "
                         << geoDoubleBlock[code] << "\n";
                  }
               }
               break;
            }

            case PROJ_FALSE_NORTHING_GEO_KEY:  // key 3083
            {
               if (tag == 34736) // using double array
               {
                  // Code is index into array.
                  if ( geoDoubleBlock && ( code < geoDoubleLength) )
                  {
                     // Always count of one.
                     out << prefix << "gtiff_false_northing: "
                         << geoDoubleBlock[code] << "\n";
                  }
               }
               break;
            }
            
            case PROJ_CENTER_LONG_GEO_KEY:  // key 3088
            {
               if (tag == 34736) // using double array
               {
                  // Code is index into array.
                  if ( geoDoubleBlock && ( code < geoDoubleLength) )
                  {
                     // Always count of one.
                     out << prefix << "gtiff_center_longitue: "
                         << geoDoubleBlock[code] << "\n";
                  }
               }
               break;
            }

            case PROJ_CENTER_LAT_GEO_KEY:  // key 3089
            {
               if (tag == 34736) // using double array
               {
                  // Code is index into array.
                  if ( geoDoubleBlock && ( code < geoDoubleLength) )
                  {
                     // Always count of one.
                     out << prefix << "gtiff_center_latitude: "
                         << geoDoubleBlock[code] << "\n";
                  }
               }
               break;
            }

            case PROJ_SCALE_AT_NAT_ORIGIN_GEO_KEY:  // key 3092
            {
               if (tag == 34736) // using double array
               {
                  // Code is index into array.
                  if ( geoDoubleBlock && ( code < geoDoubleLength) )
                  {
                     // Always count of one.
                     out << prefix << "gtiff_scale_factor: "
                         << geoDoubleBlock[code] << "\n";
                  }
               }
               break;
            }

            case VERTICAL_UNITS_GEO_KEY: // key 4099  Section 6.3.1.3 Codes
            {
               out << prefix << "gtiff_vertical_units_code: " << code << "\n";
               printLinearUnits(out, prefix, std::string("gtiff_vertical_units"), code);
               break;
            }
            
            default:
            {
               if (key > 1)
               {
                  if (tag == 34737) // using ascii array
                  {
                     if (geoAsciiBlock && ( (code+count) <= geoAsciiLength ) )
                     {
                        std::string s;
                        int i = 0;
                        while (i < count)
                        {
                           s.push_back(geoAsciiBlock[code+i]);
                           ++i;
                        }
                        out << prefix << "key_" << key << ": " << s << "\n";
                     }
                  }
                  else
                  {
                     out << prefix << "unhandle_key: " << key << "\n";
                  }
               }
               break;
            }
            
         } // matches: switch(key)
      }
   }

   return out;
}

std::ostream& GeoTiff::printModelType(std::ostream& out,
                                           std::string& prefix,
                                           drb::uint16 code) const
{
   // key 1024 Section 6.3.1.1 Codes
   out << prefix << "gtiff_model_type: ";
   if (code == 1)
   {
      out << "projected\n";
   }
   else if (code == 2)
   {
      out << "geographic\n";
   }
   else if (code == 2)
   {
      out << "geocentric\n";
   }
   else
   {
      out << "unknown\n";
   }
   return out;
}

std::ostream& GeoTiff::printRasterType(std::ostream& out,
                                            std::string& prefix,
                                            drb::uint16 code) const
{
   // key 1025 Section 6.3.1.2 Codes
   out << prefix << "gtiff_raster_type: ";
   if (code == 1)
   {
      out << "pixel_is_area\n";
   }
   else if (code == 2)
   {
      out << "pixels_is_point\n";
   }
   else
   {
      out << "unknown\n";
   }
   return out;
}

std::ostream& GeoTiff::printAngularUnits(std::ostream& out,
                                              std::string& prefix,
                                              drb::uint16 code) const
{
   // key 2054 Section 6.3.1.4 Codes
   out << prefix << "gtiff_angular_units: ";
   switch (code)
   {
      case 9101:
      {
         out << "radians\n";
         break;
      }
      case 9102:
      {
         out << "degrees\n";
         break;
      }
      case 9103:
      {
         out << "arc_minutes\n";
         break;
      }
      case 9104:
      {
         out << "arc_seconds\n";
         break;
      }
      case 9105:
      {
         out << "grad\n";
         break;
      }
      case 9106:
      {
         out << "gon\n";
         break;
      }
      case 9107:
      {
         out << "dms\n";
         break;
      }
      case 9108:
      {
         out << "dms_hemisphere\n";
         break;
      }
      default:
      {
         out << "unknown\n";
         break;
      }
   }
   return out;
}

std::ostream& GeoTiff::printCoordTrans(std::ostream& out,
                                            const std::string& prefix,
                                            drb::uint16 code) const
{
   // key 3075 Section 6.3.3.3 Codes
   out << prefix << "coord_trans: ";
   switch (code)
   {
      case 1:
      {
         out << "TransverseMercator\n";
         break;
      }
      case 2:
      {
         out << "TransvMercator_Modified_Alaska\n";
         break;
      }
      case 3:
      {
         out << "ObliqueMercator\n";
         break;
      }
      case 4:
      {
         out << "ObliqueMercator_Laborde\n";
         break;
      }
      case 5:
      {
         out << "ObliqueMercator_Rosenmund\n";
         break;
      }
      case 6:
      {
         out << "ObliqueMercator_Spherical\n";
         break;
      }
      case 7:
      {
         out << "Mercator\n";
         break;
      }
      case 8:
      {
         out << "LambertConfConic_2SP\n";
         break;
      }
      case 9:
      {
         out << "LambertConfConic_Helmert\n";
         break;
      }
      case 10:
      {
         out << "LambertAzimEqualArea\n";
         break;
      }
      case 11:
      {
         out << "AlbersEqualArea\n";
         break;
      }
      case 12:
      {
         out << "AzimuthalEquidistant\n";
         break;
      }
      case 13:
      {
         out << "EquidistantConic\n";
         break;
      }
      case 14:
      {
         out << "Stereographic\n";
         break;
      }
      case 15:
      {
         out << "PolarStereographic\n";
         break;
      }
      case 16:
      {
         out << "ObliqueStereographic\n";
         break;
      }
      case 17:
      {
         out << "Equirectangular\n";
         break;
      }
      case 18:
      {
         out << "CassiniSoldner\n";
         break;
      }
      case 19:
      {
         out << "Gnomonic\n";
         break;
      }
      case 20:
      {
         out << "MillerCylindrical\n";
         break;
      }
      case 21:
      {
         out << "Orthographic\n";
         break;
      }
      case 22:
      {
         out << "Polyconic\n";
         break;
      }
      case 23:
      {
         out << "Robinson\n";
         break;
      }
      case 24:
      {
         out << "Sinusoidal\n";
         break;
      }
      case 25:
      {
         out << "VanDerGrinten\n";
         break;
      }
      case 26:
      {
         out << "NewZealandMapGrid\n";
         break;
      }
      case 27:
      {
         out << "TransvMercator_SouthOriented\n";
         break;
      }
      default:
      {
         out << code << " unknown\n";
         break;
      }
   }
   return out;
}
   
std::ostream& GeoTiff::printLinearUnits(std::ostream& out,
                                        const std::string& prefix,
                                        const std::string& key,
                                        drb::uint16 code) const
{
   // key 3076 Section 6.3.1.3 Codes
   out << prefix << key << ": ";
   switch (code)
   {
      case 9001:
      {
         out << "meters\n";
         break;
      }
      case 9002:
      {
         out << "feet\n";
         break;
      }
      case 9003:
      {
         out << "us_survey_feet\n";
         break;
      }
      case 9004:
      {
         out << "foot_modified_american\n";
         break;
      }
      case 9005:
      {
         out << "foot_clarke\n";
         break;
      }
      case 9006:
      {
         out << "foot_indian\n";
         break;
      }
      case 9007:
      {
         out << "link\n";
         break;
      }
      case 9008:
      {
         out << "link_benoit\n";
         break;
      }
      case 9009:
      {
         out << "link_sears\n";
         break;
      }
      case 9010:
      {
         out << "chain_benoit\n";
         break;
      }
      case 9011:
      {
         out << "chain_sears\n";
         break;
      }
      case 9012:
      {
         out << "yard_sears\n";
         break;
      }
      case 9013:
      {
         out << "yard_indian\n";
         break;
      }
      case 9014:
      {
         out << "fathom\n";
         break;
      }
      case 9015:
      {
         out << "mile_international_nautical\n";
         break;
      }
      default:
      {
         out << code << "unknown\n";
         break;
      }
   }
   return out;
}

std::ostream& GeoTiff::printOrientation(std::ostream& out,
                                             const std::string& prefix,
                                             drb::uint16 code) const
{
   // Tag 274:
   out << prefix << "orientation: ";
   switch (code)
   {
      case 1:
      {
         out << "top_left\n";
         break;
      }
      case 2:
      {
         out << "top_right\n";
         break;
      }
      case 3:
      {
         out << "bottom_right\n";
         break;
      }
      case 4:
      {
         out << "bottom_left\n";
         break;
      }
      case 5:
      {
         out << "left_top\n";
         break;
      }
      case 6:
      {
         out << "right_top\n";
         break;
      }
      case 7:
      {
         out << "right_bottom\n";
         break;
      }
      case 8:
      {
         out << "left_bottom\n";
         break;
      }
      default:
      {
         out << code << " unknown\n";
         break;
      }
   }
   return out;
}

std::ostream& GeoTiff::printRpcs(std::ostream& out,
                                 const std::string&  prefix,
                                 drb::uint16 type,
                                 drb::uint64 arraySizeInBytes,
                                 drb::uint8* valueArray) const
{
   if ( valueArray )
   {
      if ( arraySizeInBytes == 92 )
      {
         switch (type)
         {
            case TIFF_DOUBLE:
            {
               float64* p = (float64*)valueArray;
               
               out << prefix << "rpc.bias_error:         " << p[0] << "\n"
                   << prefix << "rpc.rand_error:         " << p[1] << "\n"
                   << prefix << "rpc.line_off:           " << p[2] << "\n"
                   << prefix << "rpc.samp_off:           " << p[3] << "\n"
                   << prefix << "rpc.lat_off:            " << p[4] << "\n"
                   << prefix << "rpc.long_off:           " << p[5] << "\n"
                   << prefix << "rpc.height_off:         " << p[6] << "\n"
                   << prefix << "rpc.line_scale:         " << p[7] << "\n"
                   << prefix << "rpc.samp_scale:         " << p[8] << "\n"
                   << prefix << "rpc.lat_scale:          " << p[9] << "\n"
                   << prefix << "rpc.long_scale:         " << p[10] << "\n"
                   << prefix << "rpc.height_scale:       " << p[11] << "\n";

               drb::int32 i = 12;
               drb::int32 coeff = 0;

               for (coeff = 0; coeff < 20; ++coeff)
               {
                  out << prefix << "rpc.line_num_coeff_" << std::setfill('0')
                      << std::setw(2) << coeff << ": " << p[i] << "\n";
                  ++i;
               }
               for (coeff = 0; coeff < 20; ++coeff)
               {
                  out << prefix << "rpc.line_den_coeff_" << std::setfill('0')
                      << std::setw(2) << coeff << ": " << p[i] << "\n";
                  ++i;
               }
               for (coeff = 0; coeff < 20; ++coeff)
               {
                  out << prefix << "rpc.samp_num_coeff_" << std::setfill('0')
                      << std::setw(2) << coeff << ": " << p[i] << "\n";
                  ++i;
               }
               for (coeff = 0; coeff < 20; ++coeff)
               {
                  out << prefix << "rpc.samp_den_coeff_" << std::setfill('0')
                      << std::setw(2) << coeff << ": " << p[i] << "\n";
                  ++i;
               }
               break;
            }
            
            default:
            {
               out << "unhandled type: " << type << "\n";
               break;
            }
         }
      }
      else
      {
         out << "invalid count " << arraySizeInBytes << " sent to GeoTiff::printRpc" << "\n"
             << "Expecting: 92\n";
      }
   }
   else
   {
      out << "null array passed to GeoTiff::printRpc method." << "\n";
   }
   return out;
}

void GeoTiff::getDirPrefix(int32 index, std::string& prefix) const
{
   prefix += "tiff_dir";
   std::ostringstream s;
   s << index;
   prefix += s.str();
   prefix += ".";
}

} // End namespace drb
