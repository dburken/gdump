#!/bin/sh

###
# File gdump-cmake-config.sh
# Convenience script for linux/unix typically placed in out of source 
# build dir parallel to gdump then do:
# ./gdump-cmake-config.sh
# make
# 
# NOTES: 
# 1) Last line of this should point to gdump.
# 2) Installs to ${install_dir}/bin/gdump
###

# NOTE: Last line of this should point to gdump.
install_dir=${HOME}
cmake -G "Unix Makefiles" \
-DCMAKE_INSTALL_PREFIX=${install_dir} \
-DCMAKE_BUILD_TYPE=Release \
..
