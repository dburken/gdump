//----------------------------------------------------------------------------
//
// License:  See top level LICENSE.txt file.
//
// Author:  David Burken
//
// Description: gdump application
//
//----------------------------------------------------------------------------
// $Id$

#include <GeoTiff.h>
#include <iostream>
#include <cstdlib>

using namespace std;

void usage( int status )
{
   std::cout << "usage:\n\n"
             << "gdump <tiff_file>\n\n"
             << "or:\n\n"
             << "gdump -u <tiff_file>\n\n"
             << "Use the -u option to print unhandled tags.\n"
             << std::endl;

   exit( status );
}

int main(int argc, char *argv[])
{
   enum
   {
      OK    = 0,
      ERROR = 1
   };

   int result = OK;
   
   if ( (argc == 2) || (argc == 3) )
   {
      bool printUnhandled = false;
      std::string file;

      if ( argc == 2 )
      {
         file = argv[1];
      }
      else // argc == 3
      {
         std::string option = argv[1];
         if ( option == "-u" )
         {
            printUnhandled = true;
            file = argv[2];
         }
         else
         {
            std::cerr << "gdump unhandled option: " << option << std::endl;
            usage( ERROR );
         }
      }
      
      drb::GeoTiff gt;
      gt.setPrintUnhandledFlag(printUnhandled);
      if (gt.open(file))
      {
         // Dump the tiff tags.
         gt.print(cout);
      }
      else
      {
         std::cerr << "Could not open: " << file << std::endl;
         result = ERROR;
      }
   }
   else
   {
      usage( result );
   }

   return result;
}
