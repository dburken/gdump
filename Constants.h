//----------------------------------------------------------------------------
// License:  LGPL
//
// Author:  David Burken
//
// Description:  Constants for drb.
//
// $Id: Constants.h 11216 2007-06-14 21:20:55Z dburken $
//----------------------------------------------------------------------------
#ifndef drbConstants_HEADER
#define drbConstants_HEADER 1

/**
 * DLL IMPORT/EXORT SECTION
 */
#if defined(__MINGW32__) || defined(__CYGWIN__) || defined(_MSC_VER) || defined(__VISUALC__) || defined(__BORLANDC__) || defined(__WATCOMC__)
#  define DRB_DLL_EXPORT __declspec(dllexport)
#  define DRB_DLL_IMPORT __declspec(dllimport)
#  ifdef DRB_DLL_MAKINGDLL
#    define DRB_DLL_DLL       DRB_DLL_EXPORT
#    define DRB_DLL_DLL_DATA(type) DRB_DLL_EXPORT type
#  else
#    define DRB_DLL_DLL      DRB_DLL_IMPORT
#    define DRB_DLL_DLL_DATA(type) DRB_DLL_IMPORT type
#  endif
#else /* not #if defined(_MSC_VER) */
#  define DRB_DLL_EXPORT
#  define DRB_DLL_IMPORT
#  define DRB_DLL_DLL
#  define DRB_DLL_DLL_DATA(type) type
#endif /* #if defined(_MSC_VER) */

namespace drb
{
   // Primative data type typedefs
   typedef char               int8;
   typedef signed char        sint8;
   typedef unsigned char      uint8;
   
   typedef short              int16;
   typedef unsigned short     uint16;
   typedef signed short       sint16;
   
   typedef int                int32;
   typedef unsigned int       uint32;
   typedef signed int         sint32;
   
   typedef long long          int64;
   typedef unsigned long long uint64;
   typedef signed long long   sint64;
   
   typedef float              float32;
   typedef double             float64;

//---
// Integer nan kept for drbIpt.
// This should be the most negative int.
//---
#define DRB_INT_NAN ((drb::int32)0x80000000)


   /*
     Definitions for scalar type identification.
   */
   enum ScalarType
   {
      SCALAR_UNKNOWN    =  0, 
      UINT8             =  1, /**< 8 bit unsigned integer        */
      SINT8             =  2, /**< 8 bit signed integer          */
      UINT16            =  3, /**< 16 bit unsigned integer       */
      SINT16            =  4, /**< 16 bit signed integer         */
      UINT32            =  5, /**< 32 bit unsigned integer       */
      SINT32            =  6, /**< 32 bit signed integer         */
      FLOAT32           =  7, /**< 32 bit floating point         */
      FLOAT64           =  8, /**< 64 bit floating point         */
      CINT16            =  9, /**< 16 bit complex integer        */
      CINT32            = 10, /**< 32 bit complex integer        */
      CFLOAT32          = 11, /**< 32 bit complex floating point */
      CFLOAT64          = 12, /**< 64 bit complex floating point */
   };

   enum ByteOrder
   {
      DRB_LITTLE_ENDIAN = 0,
      DRB_BIG_ENDIAN    = 1
   };
   

} // namespace drb


#endif /* #ifndef drbConstants_HEADER */
